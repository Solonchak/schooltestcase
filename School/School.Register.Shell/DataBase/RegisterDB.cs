﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using School.Register.Shell.Models;

namespace School.Register.Shell.DataBase
{
    public class RegisterDB : DbContext
    {
        public DbSet<StudentGroup> Groups { get; set; }
        public DbSet<Teacher> Teachers { get; set; }
        public DbSet<Student> Students { get; set; }

        static string ConnectionString = string.Format("Data Source = (LocalDB)\\MSSQLLocalDB; AttachDbFilename={0}base.mdf;Integrated Security = True; Connect Timeout = 30", AppDomain.CurrentDomain.BaseDirectory);

        public RegisterDB()
            : base(ConnectionString)
        {

        }

        static RegisterDB singletone;
        public static RegisterDB GetInstance()
        {
            try
            {
                if (singletone == null)
                {
                    singletone = new RegisterDB();
                    singletone.Initialize();
                }
                return singletone;
            }
            catch (Exception e)
            {
                Helpers.PrintExeption.Print(e);
                System.Windows.Forms.MessageBox.Show(e.Message, "Ошибка", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                return null;
            }
        }

        public void AddTeacher(Teacher teacher)
        {
            Teachers.Add(teacher);
            this.SaveChanges();      
        }

        public void AddStudentGroup(StudentGroup studentGroup)
        {
            Groups.Add(studentGroup);
            this.SaveChanges();
        }

        public void AddStudent(Student student)
        {
            Students.Add(student);
            this.SaveChanges();
        }

        public void RemoveStudent(Student student)
        {
            RemoveStudentWithoutSaving(student);
            this.SaveChanges();
        }

        private void RemoveStudentWithoutSaving(Student student)
        {
            Students.Remove(student);
        }

        public void RemoveStudentGroup(StudentGroup studentGroup)
        {
            var _students = GetStudents(studentGroup);
            foreach (var st in _students)
            {
                RemoveStudentWithoutSaving(st);
            }
            Groups.Remove(studentGroup);
            this.SaveChanges();
        }

        public void RemoveTeacher(Teacher teacher)
        {            
            Teachers.Remove(teacher);
            this.SaveChanges();
        }

        public List<Student> GetStudents(StudentGroup studentGroup)
        {
            var students = from st in Students
                           where st.StudentGroup.Id == studentGroup.Id
                           select st;
            return students.ToList();
        }

        public Teacher GetTeacher(StudentGroup studentGroup)
        {
            var _teachers = from tchr in Teachers
                            where tchr.Group.Id == studentGroup.Id
                            select tchr;
            if (_teachers.Count() > 0)
                return _teachers.First();
            else
                return null;                        
        }

        public List<Teacher> GetTeachers()
        {
            return Teachers.ToList();
        }

        public List<StudentGroup> GetStudentGroups()
        {
            return Groups.ToList();
        }

        public List<StudentGroup> GetStudentGroupsWithoutTeacher()
        {
            var _techGroups = from _tgr in Teachers
                              select _tgr.Group;
            return Groups.Except(_techGroups).ToList();                          
        }

        void Initialize()
        {
            this.Groups.ToList();
            this.Teachers.ToList();
            this.Students.ToList();
        }
    }
}
