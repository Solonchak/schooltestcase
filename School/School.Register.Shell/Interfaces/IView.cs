﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace School.Register.Shell.Interfaces
{
    public interface IView
    {
        dynamic DataContext { get; set; }
        System.Windows.Forms.DialogResult _ShowDialog();
    }
}
