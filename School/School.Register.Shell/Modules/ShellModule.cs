﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using School.Register.Shell.Views;
using School.Register.Shell.ViewModel;
using School.Common.MVVM.Interfaces;
using Unity;

namespace School.Register.Shell.Modules
{
    
    class ShellModule : IModule
    {
        private UnityContainer container;

        public Action Closed { get; set; }

        ShellView view;
        SchoolViewModel viewModel;
        public ShellModule(UnityContainer container, ShellView view, SchoolViewModel viewModel)
        {
            this.container = container;
            this.view = view;
            this.viewModel = viewModel;            
        }

        public void Initialize()
        {
            view.DataContext = viewModel;
            view.Show();

            view.FormClosed += (sender, e) => { if (Closed != null) Closed(); };                
        }        
    }
}
