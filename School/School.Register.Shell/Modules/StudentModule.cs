﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using School.Common.MVVM.Interfaces;
using School.Register.Shell.Models;
using School.Register.Shell.Views;
using School.Register.Shell.ViewModel;

namespace School.Register.Shell.Modules
{
    class StudentModule : ModuleBase<StudentView,StudentViewModel,Student>
    {        
        public StudentModule(StudentView view, StudentViewModel viewModel) : base(view, viewModel)
        {
            
        }

        public StudentModule(Student student) : base(student)
        {
            
        }        
    }
}
