﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using School.Register.Shell.Interfaces;
using School.Common.MVVM.Interfaces;

namespace School.Register.Shell.Modules
{
    abstract class ModuleBase<View, ViewModel, Model> : IModule
    {
        public Action Closed { get; set; }
        IView view;
        ViewModel viewModel;
        public System.Windows.Forms.DialogResult dialogResult { get; set; }

        public ModuleBase(View view, ViewModel viewModel)
        {
            this.view = (IView)view;
            this.viewModel = viewModel;
        }

        public ModuleBase(Model model)
        {
            var TypeView = typeof(View);
            this.view = (IView)Activator.CreateInstance(TypeView);
            var TypeViewModel = typeof(ViewModel);
            object[] args = new object[1]; args[0] = model;
            this.viewModel = (ViewModel)Activator.CreateInstance(TypeViewModel, args);
        }

        public void Initialize()
        {            
            view.DataContext = viewModel;
            dialogResult = view._ShowDialog();
        }
    }
}
