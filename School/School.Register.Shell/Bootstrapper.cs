﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using School.Register.Shell.Views;
using Microsoft.Practices.Unity;
using Unity;
using School.Common.MVVM.Interfaces;
using School.Register.Shell.Modules;

namespace School.Register.Shell
{
    class Bootstrapper
    {
        public Action Closed;
        private UnityContainer container;
        private IModule shellModule;

        public Bootstrapper()
        {
            InitializeContainer();            
        }

        private void InitializeContainer()
        {
            container = new UnityContainer();
        }

        public void Run()
        {            
            InitializeModules();
        }

        private void InitializeModules()
        {
            shellModule = container.Resolve<ShellModule>();
            shellModule.Closed += () => Closed();            
            shellModule.Initialize();
        }        
    }
}
