﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using School.Common.MVVM;
using School.Common.MVVM.Interfaces;
using System.Windows.Forms;

namespace School.Register.Shell.ViewModel
{
    abstract class DialogViewModelBase : BindableBase
    {
        public DialogResult Result { get; set; }                
        public DelegateCommand ApplyCommand { get; set; }
        public DelegateCommand CancelCommand { get; set; }

        public DialogViewModelBase(object arg)
        {
            if (arg == null) throw new ArgumentNullException();
            
            ApplyCommand = new DelegateCommand(Apply, CanApply);
            CancelCommand = new DelegateCommand(Cancel);            
        }
        void Apply() { _Apply(); }
        bool CanApply() 
        {
            if (_CanApply != null)
            {
                return _CanApply();
            }
            return false;
        }

        protected Action _Apply;
        protected Func<bool> _CanApply;
        
        void Cancel()
        {
            Result = DialogResult.Cancel;            
        }
    }
}
