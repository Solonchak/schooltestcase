﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using School.Register.Shell.Models;
using School.Common.MVVM;
using School.Common.MVVM.Interfaces;
using System.Collections.ObjectModel;
using School.Register.Shell.DataBase;
using School.Register.Shell.Modules;
using Unity;
using System.ComponentModel;

namespace School.Register.Shell.ViewModel
{
    public class SchoolViewModel : BindableBase
    {
        private UnityContainer container;

        int? _SelectedTeacherIndex;
        public int? SelectedTeacherIndex
        {
            get { return _SelectedTeacherIndex; }
            set
            {
                try
                {
                    _SelectedTeacherIndex = value;
                    OnPropertyChanged("SelectedTeacherIndex");
                    if (SelectedTeacherIndex != null && SelectedTeacherIndex < Teachers.Count)
                    {
                        SelectedTeacher = Teachers[SelectedTeacherIndex.Value];
                    }
                    else
                    {
                        SelectedTeacher = null;
                    }
                }
                catch (Exception e)
                {
                    Helpers.PrintExeption.Print(e);
                    System.Windows.Forms.MessageBox.Show(e.Message, "Ошибка", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                }
            }
        }
        
        Teacher _SelectedTeacher;
        public Teacher SelectedTeacher
        {
            get { return  _SelectedTeacher;} set
            {
                try
                {
                    _SelectedTeacher = value; OnPropertyChanged("SelectedTeacher");
                    ChangeTeacherCommand.RaiseCanExecuteChanged();
                    RemoveTeacherCommand.RaiseCanExecuteChanged();
                }
                catch (Exception e)
                {
                    Helpers.PrintExeption.Print(e);
                    System.Windows.Forms.MessageBox.Show(e.Message, "Ошибка", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                }
            }
        }

        BindingList<StudentGroup> _StudentGroups;
        public BindingList<StudentGroup> StudentGroups
        {
            get
            {
                return _StudentGroups;
            }
            set
            {
                _StudentGroups = value;
                OnPropertyChanged("StudentGroups");
            }
        }

        public BindingList<Teacher> Teachers { get; set; }        
        public BindingList<Student> Students { get; set; }

        int? _SelectedGroupIndex;
        public int? SelectedGroupIndex
        {
            get { return  _SelectedGroupIndex;}
            set
            {
                try
                {
                    _SelectedGroupIndex = value;
                    OnPropertyChanged("SelectedGroupIndex");
                    if (SelectedGroupIndex != null && SelectedGroupIndex < StudentGroups.Count)
                    {
                        SelectedStudentGroup = StudentGroups[SelectedGroupIndex.Value];
                    }
                    else
                    {
                        SelectedStudentGroup = null;
                    }
                }
                catch (Exception e)
                {
                    Helpers.PrintExeption.Print(e);
                    System.Windows.Forms.MessageBox.Show(e.Message, "Ошибка", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                }
            }
        }

        StudentGroup _SelectedStudentGroup;
        public StudentGroup SelectedStudentGroup
        {
            get { return _SelectedStudentGroup; }
            set
            {
                try
                {
                    _SelectedStudentGroup = value;
                    OnPropertyChanged("SelectedStudentGroup");                    
                    UpdateStudents();
                    ChangeStudentGroupCommand.RaiseCanExecuteChanged();
                    RemoveStudentGroupCommand.RaiseCanExecuteChanged();
                }
                catch (Exception e)
                {
                    Helpers.PrintExeption.Print(e);
                    System.Windows.Forms.MessageBox.Show(e.Message, "Ошибка", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                }
            }
        }

        int? _SelectedStudentIndex;
        public int? SelectedStudentIndex
        {
            get { return _SelectedStudentIndex;}
            set
            {
                try
                {
                    _SelectedStudentIndex = value;
                    if (SelectedStudentIndex != null && Students != null && SelectedStudentIndex < Students.Count)
                    {
                        SelectedStudent = Students[SelectedStudentIndex.Value];
                    }
                    else
                    {
                        SelectedStudent = null;
                    }
                }
                catch (Exception e)
                {
                    Helpers.PrintExeption.Print(e);
                    System.Windows.Forms.MessageBox.Show(e.Message, "Ошибка", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
                }
            }
        }

        Student _SelectedStudent;
        public Student SelectedStudent
        {
            get
            {
                return _SelectedStudent;
            }
            set
            {
                _SelectedStudent = value;
                OnPropertyChanged("SelectedStudent");
                ChangeStudentCommand.RaiseCanExecuteChanged();
                RemoveStudentCommand.RaiseCanExecuteChanged();
            }
        }
        
        public DelegateCommand AddStudentGroupCommand { get; set; }
        public DelegateCommand AddTeacherCommand { get; set; }
        public DelegateCommand AddStudentCommand { get; set; }

        public DelegateCommand ChangeTeacherCommand { get; set; }
        public DelegateCommand ChangeStudentGroupCommand { get; set; }
        public DelegateCommand ChangeStudentCommand { get; set; }

        public DelegateCommand RemoveTeacherCommand { get; set; }
        public DelegateCommand RemoveStudentGroupCommand { get; set; }
        public DelegateCommand RemoveStudentCommand { get; set; }

        void UpdateStudentGroups()
        {
            RegisterDB data = RegisterDB.GetInstance();            
            StudentGroups.Clear();
            var groups = data.GetStudentGroups();
            foreach (var grp in groups)
            {
                StudentGroups.Add(grp);
            }
            OnPropertyChanged("StudentGroups");
        }

        void UpdateStudents()
        {
            Students.Clear();
            if (SelectedStudentGroup != null)
            {
                RegisterDB data = RegisterDB.GetInstance();                

                var _studentList = data.GetStudents(SelectedStudentGroup);
                foreach (var _st in _studentList)
                {
                    Students.Add(_st);
                }
                OnPropertyChanged("Students");                
            }            
        }

        void UpdateTeachers()
        {
            RegisterDB data = RegisterDB.GetInstance();
            Teachers.Clear();

            var _teacherList = data.GetTeachers();
            foreach (var _tchr in _teacherList)
            {
                Teachers.Add(_tchr);
            }
            OnPropertyChanged("Teachers");
        }

        void UpdateAll()
        {
            UpdateStudentGroups();
            UpdateTeachers();
            UpdateStudents();
        }

        public SchoolViewModel(UnityContainer container)
        {
            try
            {
                this.container = container;
                StudentGroups = new BindingList<StudentGroup>();
                Students = new BindingList<Student>();
                Teachers = new BindingList<Teacher>();

                UpdateAll();

                AddStudentGroupCommand = new DelegateCommand(AddStudentGroup, CanAddStudentGroup);
                AddTeacherCommand = new DelegateCommand(AddTeacher, CanAddTeacher);
                AddStudentCommand = new DelegateCommand(AddStudent, CanAddStudent);
                ChangeTeacherCommand = new DelegateCommand(ChangeTeacher, CanChangeTeacher);
                ChangeStudentGroupCommand = new DelegateCommand(ChangeStudentGroup, CanChangeStudentGroup);
                ChangeStudentCommand = new DelegateCommand(ChangeStudent, CanChangeStudent);
                RemoveTeacherCommand = new DelegateCommand(RemoveTeacher, CanRemoveTeacher);
                RemoveStudentGroupCommand = new DelegateCommand(RemoveStudentGroup, CanRemoveStudentGroup);
                RemoveStudentCommand = new DelegateCommand(RemoveStudent, CanRemoveStudent);
            }
            catch (Exception e)
            {
                Helpers.PrintExeption.Print(e);
                System.Windows.Forms.MessageBox.Show(e.Message, "Ошибка", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
            }
        }

        #region CommandMethods
        void AddStudentGroup()
        {
            try
            {
                StudentGroup studentGroup = new StudentGroup();
                StudentGroupModule module = new StudentGroupModule(studentGroup);
                module.Initialize();
                if (module.dialogResult == System.Windows.Forms.DialogResult.OK) DataBase.RegisterDB.GetInstance().AddStudentGroup(studentGroup);
                UpdateStudentGroups();                
            }
            catch (Exception e)
            {
                Helpers.PrintExeption.Print(e);
                System.Windows.Forms.MessageBox.Show(e.Message, "Ошибка", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
            }
        }

        bool CanAddStudentGroup()
        {
            return true;            
        }

        void ChangeStudentGroup()
        {
            try
            {
                StudentGroup studentGroup = SelectedStudentGroup.Clone();
                StudentGroupModule module = new StudentGroupModule(studentGroup);
                module.Initialize();
                if (module.dialogResult == System.Windows.Forms.DialogResult.OK)
                {
                    SelectedStudentGroup.Load(studentGroup);
                    DataBase.RegisterDB.GetInstance().SaveChanges();
                    UpdateStudentGroups();
                    UpdateTeachers();                    
                }
            }
            catch (Exception e)
            {
                Helpers.PrintExeption.Print(e);
                System.Windows.Forms.MessageBox.Show(e.Message, "Ошибка", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
            }
        }

        bool CanChangeStudentGroup()
        {
            return SelectedStudentGroup!=null;
        }

        void RemoveStudentGroup()
        {
            try
            {
                if (System.Windows.Forms.MessageBox.Show(string.Format("Удалить {0}?", SelectedStudentGroup.ToString()), "Удаление", System.Windows.Forms.MessageBoxButtons.YesNo, System.Windows.Forms.MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                {
                    DataBase.RegisterDB.GetInstance().RemoveStudentGroup(SelectedStudentGroup);
                    SelectedStudentGroup = null;
                    UpdateStudentGroups();
                    UpdateStudents();                    
                }
            }
            catch (Exception e)
            {
                Helpers.PrintExeption.Print(e);
                System.Windows.Forms.MessageBox.Show(e.Message, "Ошибка", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
            }
        }

        bool CanRemoveStudentGroup()
        {
            return SelectedStudentGroup != null;
        }

        void AddTeacher()
        {
            try
            {
                Teacher teacher = new Teacher();
                TeacherModule module = new TeacherModule(teacher);
                module.Initialize();
                if (module.dialogResult == System.Windows.Forms.DialogResult.OK) DataBase.RegisterDB.GetInstance().AddTeacher(teacher);
                UpdateTeachers();                                
            }
            catch (Exception e)
            {
                Helpers.PrintExeption.Print(e);
                System.Windows.Forms.MessageBox.Show(e.Message, "Ошибка", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
            }
        }

        bool CanAddTeacher()
        {
            return true;
        }

        void ChangeTeacher()
        {
            try
            {
                Teacher teacher = SelectedTeacher.Clone();
                TeacherModule module = new TeacherModule(teacher);
                module.Initialize();
                if (module.dialogResult == System.Windows.Forms.DialogResult.OK)
                {
                    SelectedTeacher.Load(teacher);
                    DataBase.RegisterDB.GetInstance().SaveChanges();
                    UpdateStudentGroups();
                    UpdateTeachers();                    
                }
            }
            catch (Exception e)
            {
                Helpers.PrintExeption.Print(e);
                System.Windows.Forms.MessageBox.Show(e.Message, "Ошибка", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
            }
        }

        bool CanChangeTeacher()
        {
            return SelectedTeacher!=null;
        }

        void RemoveTeacher()
        {
            try
            {
                if (System.Windows.Forms.MessageBox.Show(string.Format("Удалить {0}?", SelectedTeacher.ToString()), "Удаление", System.Windows.Forms.MessageBoxButtons.YesNo, System.Windows.Forms.MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                {
                    DataBase.RegisterDB.GetInstance().RemoveTeacher(SelectedTeacher);
                    SelectedTeacher = null;
                    UpdateAll();
                }
            }
            catch (Exception e)
            {
                Helpers.PrintExeption.Print(e);
                System.Windows.Forms.MessageBox.Show(e.Message, "Ошибка", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
            }
        }

        bool CanRemoveTeacher()
        {
            return SelectedTeacher != null;
        }

        void AddStudent()
        {
            try
            {
                Student student = new Student();
                StudentModule module = new StudentModule(student);
                module.Initialize();
                if (module.dialogResult == System.Windows.Forms.DialogResult.OK) DataBase.RegisterDB.GetInstance().AddStudent(student);
                UpdateStudents();                
            }
            catch (Exception e)
            {
                Helpers.PrintExeption.Print(e);
                System.Windows.Forms.MessageBox.Show(e.Message, "Ошибка", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
            }
        }

        bool CanAddStudent()
        {            
            return true;
        }

        void ChangeStudent()
        {
            try
            {
                Student student = SelectedStudent.Clone();
                StudentModule module = new StudentModule(student);
                module.Initialize();
                if (module.dialogResult == System.Windows.Forms.DialogResult.OK)
                {
                    SelectedStudent.Load(student);
                    DataBase.RegisterDB.GetInstance().SaveChanges();
                    UpdateStudents();                    
                }
            }
            catch (Exception e)
            {
                Helpers.PrintExeption.Print(e);
                System.Windows.Forms.MessageBox.Show(e.Message, "Ошибка", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
            }
        }

        bool CanChangeStudent()
        {
            return SelectedStudent!=null;
        }

        void RemoveStudent()
        {
            try
            {
                if (System.Windows.Forms.MessageBox.Show(string.Format("Удалить {0}?", SelectedStudent.ToString()), "Удаление", System.Windows.Forms.MessageBoxButtons.YesNo, System.Windows.Forms.MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                {
                    DataBase.RegisterDB.GetInstance().RemoveStudent(SelectedStudent);
                    SelectedStudent = null;
                    UpdateStudents();                    
                }
            }
            catch (Exception e)
            {
                Helpers.PrintExeption.Print(e);
                System.Windows.Forms.MessageBox.Show(e.Message, "Ошибка", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
            }
        }

        bool CanRemoveStudent()
        {
            return SelectedStudent != null;
        }
        #endregion
    }
}
