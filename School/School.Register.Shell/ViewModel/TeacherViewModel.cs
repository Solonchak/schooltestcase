﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using School.Register.Shell.Models;
using School.Register.Shell.DataBase;

namespace School.Register.Shell.ViewModel
{
    class TeacherViewModel : DialogViewModelBase
    {        
        public Teacher teacher { get; set; }
        public List<StudentGroup> StudentGroups { get; set; }                

        public TeacherViewModel(Teacher teacher) : base(teacher)
        {            
            this.teacher = teacher;

            RegisterDB data = RegisterDB.GetInstance();
            StudentGroups = data.GetStudentGroupsWithoutTeacher();
            if (this.teacher.Group != null) StudentGroups.Add(this.teacher.Group);

            teacher.PropertyChanged+=(sender,e) => ApplyCommand.RaiseCanExecuteChanged();

            _Apply += () => { Result = DialogResult.OK; };
            _CanApply += () => _TeacherCanApply();
        }
                

        bool _TeacherCanApply()
        {
            if((teacher.FirstName!=string.Empty && teacher.FirstName!=null) &&
                (teacher.LastName!=string.Empty && teacher.LastName!=null) &&
                (teacher.Patronymic!=string.Empty && teacher.Patronymic!=null) &&
                (teacher.Group!=null)) return true;
            return false;
        }        
    }
}
