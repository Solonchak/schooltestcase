﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using School.Common.MVVM;
using School.Register.Shell.Models;
using School.Register.Shell.DataBase;
using System.Windows.Forms;

namespace School.Register.Shell.ViewModel
{
    class StudentGroupViewModel : DialogViewModelBase
    {        
        public StudentGroup studentGroup { get; set; }
                

        public StudentGroupViewModel(StudentGroup studentGroup) : base(studentGroup)
        {            
            this.studentGroup = studentGroup;

            _Apply += () => { Result = DialogResult.OK; };
            _CanApply += GroupCanApply;
            studentGroup.PropertyChanged += (sender, e) => { ApplyCommand.RaiseCanExecuteChanged(); };
        }                

        bool GroupCanApply()
        {
            if (studentGroup.Level != 0 &&
                studentGroup.Letter != null &&
                studentGroup.Letter != string.Empty) return true;
            return false;
        }        
    }
}
