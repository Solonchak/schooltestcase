﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using School.Common.MVVM;
using School.Register.Shell.Models;

namespace School.Register.Shell.ViewModel
{
    class StudentViewModel : DialogViewModelBase
    {        
        public Student student { get; set; }
        public List<StudentGroup> StudentGroups { get; set; }

        string _Birthdate;
        public string Birthdate
        {
            get { return _Birthdate; }
            set
            {
                _Birthdate = value;                
                OnPropertyChanged("Birthdate");
                DateTime _date;
                if(DateTime.TryParse(Birthdate, out _date))
                {
                    student.Birthdate = _date;
                }                                
            }
        }

        public StudentViewModel(Student student) : base(student)
        {            
            StudentGroups = DataBase.RegisterDB.GetInstance().GetStudentGroups();            
            this.student = student;

            if(student.Birthdate==null) Birthdate = DateTime.Now.ToShortDateString();                        

            student.PropertyChanged += (sender, e) => ApplyCommand.RaiseCanExecuteChanged();

            this._Apply += () => { Result = DialogResult.OK; };
            this._CanApply += StudentCanApply;
        }

        bool StudentCanApply()
        {
            if ((student.FirstName != string.Empty && student.FirstName != null) &&
                (student.LastName != string.Empty && student.LastName != null) &&
                (student.Patronymic != string.Empty && student.Patronymic != null) &&
                (student.Birthdate != null) &&
                (student.StudentGroup != null)) return true;
            return false;
        }
    }
}
