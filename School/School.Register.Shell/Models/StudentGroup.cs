﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using School.Common.MVVM;

namespace School.Register.Shell.Models
{
    public class StudentGroup : BindableBase
    {
        #region Properties
        [Key]
        public int Id { get; set; }

        int _Level;
        public int Level
        {
            get { return  _Level;}
            set
            {
                _Level = value; OnPropertyChanged("Level"); 
            }
        }

        string _Letter;
        public string Letter
        {
            get { return  _Letter;} 
            set
            { 
                _Letter = value; OnPropertyChanged("Letter"); 
            }
        }

        #endregion
        public override string ToString()
        {
            return string.Format("{0}{1}", Level,Letter);
        }

        public StudentGroup Clone()
        {
            StudentGroup studentGroup = new StudentGroup();
            studentGroup.Letter = this.Letter;
            studentGroup.Level = this.Level;                    

            return studentGroup;
        }

        public void Load(StudentGroup studentGroup)
        {
            if (studentGroup == null) throw new ArgumentNullException();
            this.Letter = studentGroup.Letter;
            this.Level = studentGroup.Level;            
        }

        public override bool Equals(object _obj)
        {
            if (_obj != null && _obj is StudentGroup)
            {
                var obj = (StudentGroup)_obj;
                return this.Level==obj.Level &&
                    this.Letter == obj.Letter;
            }
            else
            {
                return false;
            }
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
