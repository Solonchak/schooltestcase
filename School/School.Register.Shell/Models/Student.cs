﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using School.Common.MVVM;

namespace School.Register.Shell.Models
{
    public class Student : BindableBase
    {
        #region Properties

        [Key]
        public int Id { get; set; }

        string _FirstName;        
        public string FirstName
        {
            get { return  _FirstName;}
            set
            { 
                if(FirstName!=value)
                {
                    _FirstName = value; OnPropertyChanged("FirstName"); 
                }
            }
        }

        string _LastName;
        public string LastName { get { return  _LastName;}
            set
            {
                if (LastName != value)
                {
                    _LastName = value; OnPropertyChanged("LastName");
                }
            }
        }

        DateTime _Birthdate;
        public DateTime Birthdate
        { get { return  _Birthdate;}
            set
            {
                if (Birthdate != value)
                {
                    _Birthdate = value; OnPropertyChanged("Birthdate");
                }
            }
        }

        string _Patronymic;
        public string Patronymic
        {
            get { return _Patronymic; }
            set
            {
                if (Patronymic != value)
                {
                    _Patronymic = value; OnPropertyChanged("Patronymic");
                }
            }
        }

        StudentGroup _StudentGroup;
        public StudentGroup StudentGroup
        {
            get { return _StudentGroup; }
            set
            {
                if (StudentGroup != value)
                {
                    _StudentGroup = value; OnPropertyChanged("StudentGroup");
                }
            }
        }
        #endregion

        public Student()
        {
            FirstName = LastName = Patronymic = string.Empty;
        }

        public Student Clone()
        {
            Student student = new Student();
            student.FirstName = this.FirstName;
            student.LastName = this.LastName;
            student.Patronymic = this.Patronymic;
            student.Birthdate = this.Birthdate;
            student.StudentGroup = this.StudentGroup;            

            return student;
        }

        public void Load(Student student)
        {
            if (student == null) throw new ArgumentNullException();
            this.FirstName = student.FirstName;
            this.LastName = student.LastName;
            this.Patronymic = student.Patronymic;
            this.Birthdate = student.Birthdate;
            this.StudentGroup = student.StudentGroup;            
        }

        public override string ToString()
        {
            return string.Format("{0} {1} {2} {3}", LastName, FirstName, Patronymic, Birthdate.ToShortDateString());
        }

        public override bool Equals(object _obj)
        {
            if (_obj != null && _obj is Student)
            {
                var obj = (Student)_obj;
               
                return  this.FirstName == obj.FirstName &&
                    this.LastName==obj.LastName &&
                    this.Patronymic==obj.Patronymic &&
                    this.Birthdate==obj.Birthdate &&
                    this.StudentGroup==obj.StudentGroup;
                    
            }
            else
            {
                return false;
            }            
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
