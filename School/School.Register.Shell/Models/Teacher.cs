﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using School.Common.MVVM;

namespace School.Register.Shell.Models
{
    public class Teacher : BindableBase
    {
        #region Properties
        [Key]
        public int Id { get; set; }
        string _FirstName;
        public string FirstName
        {
            get { return  _FirstName;} set
            { _FirstName = value; OnPropertyChanged("FirstName"); }
        }
        string _LastName;
        public string LastName
        {
            get { return  _LastName;} set
            { 
                _LastName = value; OnPropertyChanged("LastName"); 
            }
        }
        string _Patronymic;
        public string Patronymic
        {
            get { return  _Patronymic;} set
            { _Patronymic = value; OnPropertyChanged("Patronymic"); }
        }
        StudentGroup _Group;
        public StudentGroup Group
        {
            get { return _Group; }
            set
            { _Group = value; OnPropertyChanged("Group"); }
        }
        #endregion

        public Teacher Clone()
        {
            Teacher teacher = new Teacher();
            teacher.FirstName = this.FirstName;
            teacher.LastName = this.LastName;
            teacher.Patronymic = this.Patronymic;
            teacher.Group = this.Group;

            return teacher;
        }

        public void Load(Teacher teacher)
        {
            if (teacher == null) throw new ArgumentNullException();
            this.FirstName = teacher.FirstName;
            this.LastName = teacher.LastName;
            this.Patronymic = teacher.Patronymic;
            this.Group = teacher.Group;
        }

        public override string ToString()
        {
            return string.Format("{0} {1} {2}  Класс {3}", LastName, FirstName, Patronymic, Group);
        }

        public override bool Equals(object _obj)
        {            
            if (_obj!=null && _obj is Teacher)
            {               
                var obj = (Teacher)_obj;

                return this.FirstName == obj.FirstName &&
                    this.LastName == obj.LastName &&
                    this.Patronymic == obj.Patronymic &&
                    this.Group == obj.Group;                    
            }
            else
            {
                return false;
            }
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
