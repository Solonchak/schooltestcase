﻿using System;
using System.Windows.Forms;
using School.Register.Shell.Helpers;
using School.Register.Shell.Interfaces;

namespace School.Register.Shell.Views
{
    public partial class StudentGroupView : IView
    {
        dynamic _DataContext;
        public dynamic DataContext { get { return _DataContext; } set { _DataContext = value; InitializeBindings(); } }

        BindingSource bindingDataSource;

        public StudentGroupView()
        {
            InitializeComponent();
        }

        private void InitializeBindings()
        {
            try
            {
                bindingDataSource = new BindingSource();
                bindingDataSource.DataSource = DataContext;

                this.Level.DataBindings.Add(new Binding("Text", bindingDataSource, "studentGroup.Level"));
                this.Letter.DataBindings.Add(new Binding("Text", bindingDataSource, "studentGroup.Letter"));

                this.AcceptButton.DataBindings.Add(new Binding("Enabled", bindingDataSource, "ApplyCommand.IsCanExecute"));
                this.AcceptButton.Click += (sender, e) => DataContext.ApplyCommand.Execute();
                this.AcceptButton.Click += (sender, e) => _Closing();

                this.CancelButton.DataBindings.Add(new Binding("Enabled", bindingDataSource, "CancelCommand.IsCanExecute"));
                this.CancelButton.Click += (sender, e) => DataContext.CancelCommand.Execute();
                this.CancelButton.Click += (sender, e) => _Closing();                
            }
            catch(Exception e)
            {
                PrintExeption.Print(e);
                MessageBox.Show(e.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        void _Closing()
        {
            this.DialogResult = DataContext.Result;
            this.Close();
        }

        public System.Windows.Forms.DialogResult _ShowDialog()
        {
            return this.ShowDialog();
        }
    }
}
