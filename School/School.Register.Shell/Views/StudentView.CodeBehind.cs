﻿using System;
using System.Data;
using System.Windows.Forms;
using School.Register.Shell.Helpers;
using School.Register.Shell.Interfaces;

namespace School.Register.Shell.Views
{
    public partial class StudentView  : IView
    {
        dynamic _DataContext;
        public dynamic DataContext { get { return _DataContext; } set { _DataContext = value; InitializeBindings(); } }

        BindingSource bindingDataSource;
        BindingSource groupsDataSource;

        public StudentView()
        {
            InitializeComponent();
        }

        void InitializeBindings()
        {
            try
            {
                bindingDataSource = new BindingSource();                
                bindingDataSource.DataSource = DataContext;

                groupsDataSource = new BindingSource();
                groupsDataSource.DataSource = bindingDataSource;
                groupsDataSource.DataMember = "StudentGroups";

                this.StudentGroupCb.DataSource = groupsDataSource;
                this.StudentGroupCb.DataBindings.Add(new Binding("SelectedItem", bindingDataSource, "student.StudentGroup"));

                this.FirstName.DataBindings.Add(new Binding("Text", bindingDataSource, "student.FirstName"));
                this.LastName.DataBindings.Add(new Binding("Text", bindingDataSource, "student.LastName"));
                this.Patronymic.DataBindings.Add(new Binding("Text", bindingDataSource, "student.Patronymic"));
                this.Birthdate.DataBindings.Add(new Binding("Text", bindingDataSource, "Birthdate"));                
                                                
                this.AcceptButton.DataBindings.Add(new Binding("Enabled", bindingDataSource, "ApplyCommand.IsCanExecute"));
                this.AcceptButton.Click += (sender, e) => DataContext.ApplyCommand.Execute();
                this.AcceptButton.Click += (sender, e) => _Closing();
                
                this.CancelButton.DataBindings.Add(new Binding("Enabled", bindingDataSource, "CancelCommand.IsCanExecute"));
                this.CancelButton.Click += (sender, e) => DataContext.CancelCommand.Execute();
                this.CancelButton.Click += (sender, e) => _Closing();                                

            }
            catch (Exception e)
            {
                PrintExeption.Print(e);
                MessageBox.Show(e.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public System.Windows.Forms.DialogResult _ShowDialog()
        {
            return this.ShowDialog();
        }

        void _Closing()
        {
            this.DialogResult = DataContext.Result;
            this.Close();
        }
    }
}
