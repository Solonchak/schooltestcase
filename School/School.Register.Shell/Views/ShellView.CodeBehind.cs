﻿using System;
using System.Windows.Forms;
using School.Register.Shell.Helpers;

namespace School.Register.Shell.Views
{
    public partial class ShellView
    {
        dynamic _DataContext;
        public dynamic DataContext { get { return _DataContext; } set { _DataContext = value; InitializeBindings(); } }
        /*BindingSource bindingDataSource;
        BindingSource studentGroupsSource;
        BindingSource studentsSource;
        */
        public ShellView()
        {
            InitializeComponent();            
        }

        private void InitializeBindings()
        {
            try
            {
                /*bindingDataSource = new BindingSource();
                bindingDataSource.DataSource = DataContext;

                studentGroupsSource = new BindingSource();
                studentGroupsSource.DataSource = bindingDataSource;
                studentGroupsSource.DataMember = "StudentGroups";

                studentsSource = new BindingSource();
                studentsSource.DataSource = bindingDataSource;
                studentsSource.DataMember = "Students";*/

                this.GroupList.DataSource = DataContext.StudentGroups;                                
                this.GroupList.SelectionChanged += (sender, e) => SelectedGroupChanged();

                this.TeacherList.DataSource = DataContext.Teachers;
                this.TeacherList.SelectionChanged += (sender, e) => SelectedTeacherChanged();

                this.StudentList.SelectionChanged += (sender, e) => SelectedStudentChanged();

                this.TeacherName.DataBindings.Add(new Binding("Text", DataContext, "SelectedTeacher"));

                this.AddGroup.DataBindings.Add(new Binding("Enabled", DataContext, "AddStudentGroupCommand.IsCanExecute"));
                this.AddGroup.Click += (sender, e) => DataContext.AddStudentGroupCommand.Execute();

                this.AddTeacher.DataBindings.Add(new Binding("Enabled", DataContext, "AddTeacherCommand.IsCanExecute"));
                this.AddTeacher.Click += (sender, e) => DataContext.AddTeacherCommand.Execute();

                this.AddStudent.DataBindings.Add(new Binding("Enabled", DataContext, "AddStudentCommand.IsCanExecute"));
                this.AddStudent.Click += (sender, e) => DataContext.AddStudentCommand.Execute();

                this.ChangeTeacher.DataBindings.Add(new Binding("Enabled", DataContext, "ChangeTeacherCommand.IsCanExecute"));
                this.ChangeTeacher.Click += (sender, e) => DataContext.ChangeTeacherCommand.Execute();

                this.ChangeStudentGroup.DataBindings.Add(new Binding("Enabled", DataContext, "ChangeStudentGroupCommand.IsCanExecute"));
                this.ChangeStudentGroup.Click += (sender, e) => DataContext.ChangeStudentGroupCommand.Execute();

                this.ChangeStudent.DataBindings.Add(new Binding("Enabled", DataContext, "ChangeStudentCommand.IsCanExecute"));
                this.ChangeStudent.Click += (sender, e) => DataContext.ChangeStudentCommand.Execute();

                this.RemoveTeacher.DataBindings.Add(new Binding("Enabled", DataContext, "RemoveTeacherCommand.IsCanExecute"));
                this.RemoveTeacher.Click += (sender, e) => DataContext.RemoveTeacherCommand.Execute();

                this.RemoveStudentGroup.DataBindings.Add(new Binding("Enabled", DataContext, "RemoveStudentGroupCommand.IsCanExecute"));
                this.RemoveStudentGroup.Click += (sender, e) => DataContext.RemoveStudentGroupCommand.Execute();

                this.RemoveStudent.DataBindings.Add(new Binding("Enabled", DataContext, "RemoveStudentCommand.IsCanExecute"));
                this.RemoveStudent.Click += (sender, e) => DataContext.RemoveStudentCommand.Execute();

                /*this.GroupListMenu.DataBindings.Add(new Binding("Enabled", DataContext, "ChangeStudentGroupCommand.IsCanExecute"));
                this.GroupMenuChangeButton.Click+= (sender, e) => DataContext.ChangeStudentGroupCommand.Execute();
                
                this.StudentListMenu.DataBindings.Add(new Binding("Enabled", DataContext, "ChangeStudentCommand.IsCanExecute"));
                this.StudentMenuChangeButton.Click += (sender, e) => DataContext.ChangeStudentCommand.Execute();*/
            }
            catch (Exception e)
            {
                PrintExeption.Print(e);
                MessageBox.Show(e.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        void SelectedTeacherChanged()
        {
            try
            {
                if (this.TeacherList.SelectedRows.Count != 0)
                {
                    DataContext.SelectedTeacherIndex = this.TeacherList.SelectedRows[0].Index;
                    //RefreshBindings();
                }
                else
                {
                    DataContext.SelectedTeacherIndex = null;
                }
            }
            catch (Exception e)
            {
                PrintExeption.Print(e);
                MessageBox.Show(e.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        void SelectedGroupChanged()
        {
            try
            {
                if (this.GroupList.SelectedRows.Count != 0)
                {
                    DataContext.SelectedGroupIndex = this.GroupList.SelectedRows[0].Index;
                    RefreshBindings();
                }
                else
                {
                    DataContext.SelectedGroupIndex = null;
                }
            }
            catch(Exception e)
            {
                PrintExeption.Print(e);
                MessageBox.Show(e.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        void SelectedStudentChanged()
        {
            try
            {
                if (this.StudentList.SelectedRows.Count != 0)
                {
                    DataContext.SelectedStudentIndex = this.StudentList.SelectedRows[0].Index;                    
                }
                else
                {
                    DataContext.SelectedStudentIndex = null;
                }
            }
            catch (Exception e)
            {
                PrintExeption.Print(e);
                MessageBox.Show(e.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void RefreshBindings()
        {
            try
            {                
                //Список учеников
                if (DataContext.SelectedStudentGroup != null)
                {                    
                    this.StudentList.DataSource = DataContext.Students;
                }
            }
            catch (Exception e)
            {
                PrintExeption.Print(e);
                MessageBox.Show(e.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
