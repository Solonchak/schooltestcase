﻿namespace School.Register.Shell.Views
{
    partial class ShellView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.GroupList = new System.Windows.Forms.DataGridView();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.TeacherList = new System.Windows.Forms.DataGridView();
            this.RemoveTeacher = new System.Windows.Forms.Button();
            this.ChangeTeacher = new System.Windows.Forms.Button();
            this.AddTeacher = new System.Windows.Forms.Button();
            this.RemoveStudentGroup = new System.Windows.Forms.Button();
            this.ChangeStudentGroup = new System.Windows.Forms.Button();
            this.AddGroup = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.TeacherName = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.RemoveStudent = new System.Windows.Forms.Button();
            this.ChangeStudent = new System.Windows.Forms.Button();
            this.AddStudent = new System.Windows.Forms.Button();
            this.StudentList = new System.Windows.Forms.DataGridView();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GroupList)).BeginInit();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TeacherList)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.StudentList)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.RemoveStudentGroup);
            this.groupBox1.Controls.Add(this.ChangeStudentGroup);
            this.groupBox1.Controls.Add(this.AddGroup);
            this.groupBox1.Controls.Add(this.GroupList);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(141, 501);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Список классов";
            // 
            // GroupList
            // 
            this.GroupList.AllowUserToAddRows = false;
            this.GroupList.AllowUserToDeleteRows = false;
            this.GroupList.AllowUserToResizeColumns = false;
            this.GroupList.AllowUserToResizeRows = false;
            this.GroupList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.GroupList.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.GroupList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GroupList.Location = new System.Drawing.Point(6, 106);
            this.GroupList.MultiSelect = false;
            this.GroupList.Name = "GroupList";
            this.GroupList.ReadOnly = true;
            this.GroupList.RowHeadersVisible = false;
            this.GroupList.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.GroupList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.GroupList.Size = new System.Drawing.Size(125, 390);
            this.GroupList.TabIndex = 0;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.RemoveTeacher);
            this.groupBox6.Controls.Add(this.ChangeTeacher);
            this.groupBox6.Controls.Add(this.AddTeacher);
            this.groupBox6.Controls.Add(this.TeacherList);
            this.groupBox6.Location = new System.Drawing.Point(543, 12);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(308, 501);
            this.groupBox6.TabIndex = 11;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Учителя";
            // 
            // TeacherList
            // 
            this.TeacherList.AllowUserToAddRows = false;
            this.TeacherList.AllowUserToDeleteRows = false;
            this.TeacherList.AllowUserToResizeColumns = false;
            this.TeacherList.AllowUserToResizeRows = false;
            this.TeacherList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.TeacherList.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.TeacherList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.TeacherList.Location = new System.Drawing.Point(3, 106);
            this.TeacherList.MultiSelect = false;
            this.TeacherList.Name = "TeacherList";
            this.TeacherList.ReadOnly = true;
            this.TeacherList.RowHeadersVisible = false;
            this.TeacherList.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.TeacherList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.TeacherList.Size = new System.Drawing.Size(300, 390);
            this.TeacherList.TabIndex = 1;
            // 
            // RemoveTeacher
            // 
            this.RemoveTeacher.Location = new System.Drawing.Point(6, 77);
            this.RemoveTeacher.Name = "RemoveTeacher";
            this.RemoveTeacher.Size = new System.Drawing.Size(296, 23);
            this.RemoveTeacher.TabIndex = 12;
            this.RemoveTeacher.Text = "Удалить классного руководителя";
            this.RemoveTeacher.UseVisualStyleBackColor = true;
            // 
            // ChangeTeacher
            // 
            this.ChangeTeacher.Location = new System.Drawing.Point(6, 48);
            this.ChangeTeacher.Name = "ChangeTeacher";
            this.ChangeTeacher.Size = new System.Drawing.Size(296, 23);
            this.ChangeTeacher.TabIndex = 11;
            this.ChangeTeacher.Text = "Редактировать классного руководителя";
            this.ChangeTeacher.UseVisualStyleBackColor = true;
            // 
            // AddTeacher
            // 
            this.AddTeacher.Location = new System.Drawing.Point(6, 19);
            this.AddTeacher.Name = "AddTeacher";
            this.AddTeacher.Size = new System.Drawing.Size(296, 23);
            this.AddTeacher.TabIndex = 10;
            this.AddTeacher.Text = "Добавить классного руководителя";
            this.AddTeacher.UseVisualStyleBackColor = true;
            // 
            // RemoveStudentGroup
            // 
            this.RemoveStudentGroup.Location = new System.Drawing.Point(6, 77);
            this.RemoveStudentGroup.Name = "RemoveStudentGroup";
            this.RemoveStudentGroup.Size = new System.Drawing.Size(125, 23);
            this.RemoveStudentGroup.TabIndex = 13;
            this.RemoveStudentGroup.Text = "Удалить класс";
            this.RemoveStudentGroup.UseVisualStyleBackColor = true;
            // 
            // ChangeStudentGroup
            // 
            this.ChangeStudentGroup.Location = new System.Drawing.Point(6, 48);
            this.ChangeStudentGroup.Name = "ChangeStudentGroup";
            this.ChangeStudentGroup.Size = new System.Drawing.Size(125, 23);
            this.ChangeStudentGroup.TabIndex = 12;
            this.ChangeStudentGroup.Text = "Редактировать класс";
            this.ChangeStudentGroup.UseVisualStyleBackColor = true;
            // 
            // AddGroup
            // 
            this.AddGroup.Location = new System.Drawing.Point(6, 19);
            this.AddGroup.Name = "AddGroup";
            this.AddGroup.Size = new System.Drawing.Size(125, 23);
            this.AddGroup.TabIndex = 11;
            this.AddGroup.Text = "Добавить класс";
            this.AddGroup.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.TeacherName);
            this.groupBox4.Location = new System.Drawing.Point(458, 584);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(376, 52);
            this.groupBox4.TabIndex = 13;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Классный руководитель";
            // 
            // TeacherName
            // 
            this.TeacherName.Location = new System.Drawing.Point(6, 19);
            this.TeacherName.Name = "TeacherName";
            this.TeacherName.ReadOnly = true;
            this.TeacherName.Size = new System.Drawing.Size(364, 20);
            this.TeacherName.TabIndex = 2;
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.groupBox3.Controls.Add(this.RemoveStudent);
            this.groupBox3.Controls.Add(this.ChangeStudent);
            this.groupBox3.Controls.Add(this.AddStudent);
            this.groupBox3.Controls.Add(this.StudentList);
            this.groupBox3.Location = new System.Drawing.Point(159, 12);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(378, 498);
            this.groupBox3.TabIndex = 14;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Ученики";
            // 
            // RemoveStudent
            // 
            this.RemoveStudent.Location = new System.Drawing.Point(6, 77);
            this.RemoveStudent.Name = "RemoveStudent";
            this.RemoveStudent.Size = new System.Drawing.Size(366, 23);
            this.RemoveStudent.TabIndex = 11;
            this.RemoveStudent.Text = "Удалить ученика";
            this.RemoveStudent.UseVisualStyleBackColor = true;
            // 
            // ChangeStudent
            // 
            this.ChangeStudent.Location = new System.Drawing.Point(6, 48);
            this.ChangeStudent.Name = "ChangeStudent";
            this.ChangeStudent.Size = new System.Drawing.Size(366, 23);
            this.ChangeStudent.TabIndex = 10;
            this.ChangeStudent.Text = "Редактировать ученика";
            this.ChangeStudent.UseVisualStyleBackColor = true;
            // 
            // AddStudent
            // 
            this.AddStudent.Location = new System.Drawing.Point(6, 19);
            this.AddStudent.Name = "AddStudent";
            this.AddStudent.Size = new System.Drawing.Size(366, 23);
            this.AddStudent.TabIndex = 9;
            this.AddStudent.Text = "Добавить ученика";
            this.AddStudent.UseVisualStyleBackColor = true;
            // 
            // StudentList
            // 
            this.StudentList.AllowUserToAddRows = false;
            this.StudentList.AllowUserToDeleteRows = false;
            this.StudentList.AllowUserToResizeColumns = false;
            this.StudentList.AllowUserToResizeRows = false;
            this.StudentList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.StudentList.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.StudentList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.StudentList.Location = new System.Drawing.Point(6, 106);
            this.StudentList.MultiSelect = false;
            this.StudentList.Name = "StudentList";
            this.StudentList.ReadOnly = true;
            this.StudentList.RowHeadersVisible = false;
            this.StudentList.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.StudentList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.StudentList.Size = new System.Drawing.Size(366, 390);
            this.StudentList.TabIndex = 1;
            // 
            // ShellView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(857, 517);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "ShellView";
            this.Text = "Школа";
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GroupList)).EndInit();
            this.groupBox6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TeacherList)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.StudentList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView GroupList;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.DataGridView TeacherList;
        private System.Windows.Forms.Button RemoveStudentGroup;
        private System.Windows.Forms.Button ChangeStudentGroup;
        private System.Windows.Forms.Button AddGroup;
        private System.Windows.Forms.Button RemoveTeacher;
        private System.Windows.Forms.Button ChangeTeacher;
        private System.Windows.Forms.Button AddTeacher;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox TeacherName;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button RemoveStudent;
        private System.Windows.Forms.Button ChangeStudent;
        private System.Windows.Forms.Button AddStudent;
        private System.Windows.Forms.DataGridView StudentList;
    }
}