﻿using System;
using System.Windows.Forms;
using School.Register.Shell.Helpers;
using School.Register.Shell.Interfaces;

namespace School.Register.Shell.Views
{
    public partial class TeacherView : IView
    {
        dynamic _DataContext;
        public dynamic DataContext { get { return _DataContext; } set { _DataContext = value; InitializeBindings(); } }        
        public TeacherView()
        {
            InitializeComponent();
        }

        BindingSource bindingDataSource;
        BindingSource groupsDataSource;

        void InitializeBindings()
        {
            try
            {
                bindingDataSource = new BindingSource();
                bindingDataSource.DataSource = DataContext;

                groupsDataSource = new BindingSource();
                groupsDataSource.DataSource = bindingDataSource;
                groupsDataSource.DataMember = "StudentGroups";

                this.FirstName.DataBindings.Add(new Binding("Text", bindingDataSource, "teacher.FirstName"));
                this.LastName.DataBindings.Add(new Binding("Text", bindingDataSource, "teacher.LastName"));
                this.Patronymic.DataBindings.Add(new Binding("Text", bindingDataSource, "teacher.Patronymic"));

                this.StudentGroupCB.DataSource = groupsDataSource;
                this.StudentGroupCB.DataBindings.Add(new Binding("SelectedItem", bindingDataSource, "teacher.Group"));

                this.AcceptButton.DataBindings.Add(new Binding("Enabled", bindingDataSource, "ApplyCommand.IsCanExecute"));
                this.AcceptButton.Click += (sender, e) => DataContext.ApplyCommand.Execute();
                this.AcceptButton.Click += (sender, e) => _Closing();

                this.CancelButton.DataBindings.Add(new Binding("Enabled", bindingDataSource, "CancelCommand.IsCanExecute"));
                this.CancelButton.Click += (sender, e) => DataContext.CancelCommand.Execute();
                this.CancelButton.Click += (sender, e) => _Closing();                
            }
            catch (Exception e)
            {
                PrintExeption.Print(e);
                MessageBox.Show(e.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public System.Windows.Forms.DialogResult _ShowDialog()
        {
            return this.ShowDialog();
        }

        void _Closing()
        {            
            this.DialogResult = DataContext.Result;
            this.Close();
        }
    }
}
