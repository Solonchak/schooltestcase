﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;

namespace School.Register.Shell.Helpers
{
    class PrintExeption
    {
        public static void Print(Exception e)
        {
            ILogger logger = LogManager.GetCurrentClassLogger();
            logger.Fatal("{0} Stack Trace {1} Message {2}", e.TargetSite, e.StackTrace, e.Message);
            if (e.InnerException != null)
            {
                logger.Fatal("Inner Message{0}", e.InnerException.Message);
            }            
        }
    }
}
