﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using School.Register.Shell.Models;

namespace School.Register.Shell.UnitTests
{
    [TestFixture]
    class TeacherTests
    {
        Teacher Make()
        {
            var obj = new Teacher() { FirstName = "Иван", LastName = "Иванов", Patronymic = "Иванович", Group = new StudentGroup()};
            return obj;
        }

        [Test]
        public void Clone_ByDefault_ReturnCorrectObj()
        {
            var expect = Make();

            var result = expect.Clone();

            Assert.AreEqual(expect, result);
        }

        [Test]
        public void Load_WithArg_CorrectObj()
        {
            var expect = Make();

            var result = new Teacher();
            result.Load(expect);

            Assert.AreEqual(expect, result);
        }

        [Test]
        public void Load_WithNullArg_Throw()
        {
            var obj = Make();

            var ex = Assert.Catch<ArgumentNullException>(() => obj.Load(null));

            Assert.NotNull(ex);
        }
    }
}
