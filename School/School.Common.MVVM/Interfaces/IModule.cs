﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace School.Common.MVVM.Interfaces
{
    public interface IModule
    {
        void Initialize();
        Action Closed { get; set; }        
    }
}
