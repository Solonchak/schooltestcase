﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace School.Common.MVVM
{
    public class DelegateCommand : BindableBase
    {
           
        public Action Execute { get; private set; }
        public Func<bool> CanExecute { get; private set; }

        bool _isCanExecute;
        public bool IsCanExecute
        {
            get { return _isCanExecute; }
            set
            { _isCanExecute = value; OnPropertyChanged("IsCanExecute"); }
        }


        public DelegateCommand(Action Execute)
        {
            this.Execute = Execute;
            IsCanExecute = true;
        }

        public DelegateCommand(Action Execute, Func<bool> CanExecute)
        {
            this.Execute = Execute;
            this.CanExecute = CanExecute;
            RaiseCanExecuteChanged();
        }

        public void RaiseCanExecuteChanged()
        {
            if (CanExecute != null)
                IsCanExecute = CanExecute();            
        }

    }
}
